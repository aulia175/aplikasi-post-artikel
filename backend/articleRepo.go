package main

import (
	"database/sql"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

func dbConn() (db *sql.DB) {
	dbDriver := "mysql"
	dbUser := "root"
	dbPass := ""
	dbName := "article_app"
	db, err := sql.Open(dbDriver, dbUser+":"+dbPass+"@/"+dbName)
	if err != nil {
		panic(err.Error())
	}
	// fmt.Println("koneksi ke mysql berhasil dibuat")
	return db
}

func findOne(id string) ArticleOut {
	db := dbConn()
	defer db.Close()

	frmDB, err := db.Query("SELECT * FROM posts WHERE id=?", id)
	if err != nil {
		panic(err.Error())
	}
	target := ArticleOut{}

	for frmDB.Next() {
		var title, content, category, status, createdDate, updatedDate string
		err = frmDB.Scan(&id, &title, &content, &category, &createdDate, &updatedDate, &status)
		if err != nil {
			panic(err.Error())
		}
		target.Title = title
		target.Content = content
		target.Category = category
		target.Status = status
	}
	return target
}

func findAll(limit string, offset string) []ArticleOut {
	db := dbConn()
	defer db.Close()
	// fmt.Println("offset :", offset)
	// fmt.Println("limit :", limit)

	frmDB, err := db.Query("SELECT * FROM posts ORDER BY id LIMIT ?,? ", offset, limit)
	if err != nil {
		panic(err.Error())
	}
	target := []ArticleOut{}

	for frmDB.Next() {
		articleIteration := ArticleOut{}
		var id, title, content, category, status, createdDate, updatedDate string
		err = frmDB.Scan(&id, &title, &content, &category, &createdDate, &updatedDate, &status)
		if err != nil {
			panic(err.Error())
		}
		articleIteration.Title = title
		articleIteration.Content = content
		articleIteration.Category = category
		articleIteration.Status = status
		target = append(target, articleIteration)
	}
	return target
}

func insert(newArticle Article) {
	db := dbConn()
	defer db.Close()
	now := time.Now()

	_, err := db.Query("INSERT INTO posts(title,content,category,created_date,updated_date,status) VALUES (?,?,?,?,?,?)", newArticle.Title, newArticle.Content, newArticle.Category, now, now, newArticle.Status)
	if err != nil {
		panic(err.Error())
	}
}

func edit(targetArticle Article) {
	db := dbConn()
	defer db.Close()
	now := time.Now()

	_, err := db.Query("UPDATE posts SET title=? ,content=? ,category=?,updated_date=?,status=? WHERE id = ?", targetArticle.Title, targetArticle.Content, targetArticle.Category, now, targetArticle.Status, targetArticle.Id)
	if err != nil {
		panic(err.Error())
	}
}

func delete(id string) {
	db := dbConn()
	defer db.Close()

	_, err := db.Query("DELETE FROM posts WHERE id = ?", id)
	if err != nil {
		panic(err.Error())
	}
}
