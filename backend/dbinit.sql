CREATE DATABASE article_app
USE article_app
CREATE TABLE IF NOT EXISTS posts (
  id INT  AUTO_INCREMENT ,
  title VARCHAR(200) ,
  content TEXT ,
  category VARCHAR(100) ,
  created_date TIMESTAMP ,
  updated_date TIMESTAMP ,
  status VARCHAR(100) ,
  PRIMARY KEY (id) );