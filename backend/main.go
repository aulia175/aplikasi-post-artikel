package main

import (
	"fmt"
	"log"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
)

func handleRequest() {

	myRouter := mux.NewRouter().StrictSlash(true)

	myRouter.HandleFunc("/", homePage)
	myRouter.HandleFunc("/article/", addArticle).Methods("POST")
	myRouter.HandleFunc("/article/{limit}/{offset}", getAllArticleWithParam).Methods("GET")
	myRouter.HandleFunc("/article/{id}", getOneArticle).Methods("GET")
	myRouter.HandleFunc("/article/{id}", editArticle).Methods("PUT")
	myRouter.HandleFunc("/article/{id}", deleteArticle).Methods("DELETE")
	log.Fatal(http.ListenAndServe(":8081", myRouter))
}

func homePage(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "Homepage Endpoint Hit", r)
}

func main() {

	fmt.Println("app start")
	//connect db
	// connectDb()
	handleRequest()
}
