package main

import "time"

type Article struct {
	Id          string    `json:"id"`
	Title       string    `json:"Title"`
	Content     string    `json:"Content"`
	Category    string    `json:"Category"`
	Status      string    `json:"Status"`
	CreatedDate time.Time `json:"CreatedDate"`
	UpdatedDate time.Time `json:"UpdatedDate"`
}

type ArticleOut struct {
	Title    string `json:"Title"`
	Content  string `json:"Content"`
	Category string `json:"Category"`
	Status   string `json:"Status"`
}
