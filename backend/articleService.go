package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	_ "github.com/go-sql-driver/mysql"
)

func addArticle(w http.ResponseWriter, r *http.Request) {
	w = setHeader(w)
	fmt.Println("masuk addarticle")
	decoder := json.NewDecoder(r.Body)
	var target Article
	err := decoder.Decode(&target)
	if err != nil {
		panic(err)
	}
	flagValidation := checkArticle(target)
	if flagValidation {
		fmt.Println("proses insert article")
		insert(target)
	}
	json.NewEncoder(w).Encode("{}")
}

func getOneArticle(w http.ResponseWriter, r *http.Request) {

	//pasti /article/{id}
	id := strings.Split(r.URL.String(), "/")[2]
	target := findOne(id)

	w = setHeader(w)
	json.NewEncoder(w).Encode(target)

}

func getAllArticleWithParam(w http.ResponseWriter, r *http.Request) {
	limit := strings.Split(r.URL.String(), "/")[2]
	offset := strings.Split(r.URL.String(), "/")[3]
	// fmt.Println(r.URL.String())
	target := findAll(limit, offset)

	w = setHeader(w)
	json.NewEncoder(w).Encode(target)
}

func editArticle(w http.ResponseWriter, r *http.Request) {
	decoder := json.NewDecoder(r.Body)
	var target Article
	err := decoder.Decode(&target)
	if err != nil {
		panic(err)
	}
	id := strings.Split(r.URL.String(), "/")[2]
	target.Id = id
	flagValidation := checkArticle(target)
	if flagValidation {
		edit(target)

	}

	w = setHeader(w)
	json.NewEncoder(w).Encode("{}")
}

func deleteArticle(w http.ResponseWriter, r *http.Request) {
	id := strings.Split(r.URL.String(), "/")[2]
	delete(id)

	w = setHeader(w)
	json.NewEncoder(w).Encode("{}")
}

func checkArticle(article Article) bool {
	output := true
	articleStatus := article.Status

	titleFlag := len(article.Title) >= 20
	contentFlag := len(article.Content) >= 200
	categoryFlag := len(article.Category) > 3
	statusFlag := articleStatus == "publish" ||
		articleStatus == "draft" ||
		articleStatus == "thrash"

	if !titleFlag || !contentFlag || !categoryFlag || !statusFlag {
		output = false
	}
	// fmt.Println(titleFlag, contentFlag, categoryFlag, statusFlag)
	return output
}

func setHeader(w http.ResponseWriter) http.ResponseWriter {
	w.Header().Set("Content-Type", "application/json")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	return w
}
