import React, { useState } from 'react'
import {
  CButton,
  CCard,
  CCardBody,
  CCardHeader,
  CCol,
  CForm,
  CFormCheck,
  CFormControl,
  CFormFeedback,
  CFormLabel,
  CFormSelect,
  CInputGroup,
  CInputGroupText,
  CRow,
} from '@coreui/react'

const CustomStyles = () => {
  const [validated, setValidated] = useState(false)
  const newPost = {}
  const handleSubmit = (event) => {
    const form = event.currentTarget
    if (form.checkValidity() === false) {
      event.preventDefault()
      event.stopPropagation()
    }
    else{
        event.preventDefault()
        newPost.title = form.titleInput.value
        newPost.category = form.categoryInput.value
        newPost.content = form.contentInput.value
        const axios = require("axios");
        const url = "http://localhost:8081/article/";
        axios.defaults.headers.post['Access-Control-Allow-Origin'] = '*';
        axios.post(url, newPost)
            .then(response => console.log(response))
            .catch((error) => {
                console.error("Error", error);
              });
        console.log(newPost)
    }
    
    setValidated(true)
  }
  return (
    <CForm
      className="row g-3 needs-validation"
      noValidate
      validated={validated}
      onSubmit={handleSubmit}
    >
      <CCol md="6">
        <CFormLabel htmlFor="titleInput">Title</CFormLabel>
        <CFormControl type="text" id="titleInput" required />
        <CFormFeedback valid>Ok</CFormFeedback>
        <CFormFeedback invalid>Please insert title.</CFormFeedback>
      </CCol>
      <CCol md="6">
        <CFormLabel htmlFor="categoryInput">Category</CFormLabel>
        <CFormControl type="text" id="categoryInput" required />
        <CFormFeedback valid>Ok</CFormFeedback>
        <CFormFeedback invalid>Please insert category.</CFormFeedback>
      </CCol>
      <CCol md="12">
        <CFormLabel htmlFor="contentInput">Content</CFormLabel>
        <CFormControl type="textarea" id="contentInput" rows="3" required />
        <CFormFeedback valid>Ok</CFormFeedback>
        <CFormFeedback invalid>Please insert content.</CFormFeedback>
      </CCol>
      <CCol xs="2">
        <CButton color="primary" type="submit">
          Publish
        </CButton>
      </CCol>
      <CCol xs="2">
        <CButton color="secondary" type="submit">
          Draft
        </CButton>
      </CCol>
    </CForm>
  )
}

const addPost = () => {
    
  return (
    <CRow>
      <CCol xs={12}>
        <CCard className="mb-4">
          <CCardHeader>
            <strong>Validation</strong> <small>Custom styles</small>
          </CCardHeader>
          <CCardBody>
              {CustomStyles()}
          </CCardBody>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default addPost
